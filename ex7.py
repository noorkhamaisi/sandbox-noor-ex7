import os
import json
import logging
import argparse

from flask import Flask, request, Response
from ex6APP import func
from firebase_admin import initialize_app
from firebase_admin import db


# Get the projectID
PROJECT_ID = 'seetree-proto-dev'

print(f"Project : {PROJECT_ID}")

firebase_app = initialize_app(options={'databaseURL': "https://{}.firebaseio.com".format(PROJECT_ID)})

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/cipo/<farmID>/<surveyID>/<dst>',methods=['GET'])
def get_data(farmID,surveyID,dst):
    func(farmID,surveyID,dst)
    return Response("DONE", status=200)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))