# For more information, please refer to https://aka.ms/vscode-docker-python
FROM google/cloud-sdk:313.0.0

LABEL Name=$BITBUCKET_REPO_SLUG Version=0.0.1
EXPOSE 200

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Install pip requirements
ADD requirements.txt .
ADD requirements-private.txt .


RUN python3.8 -m pip install --upgrade pip
RUN python3.8 -m pip install -r requirements.txt
RUN python3.8 -m pip install -i https://seetree:suntreat2017@seetree-pip.uc.r.appspot.com/pypi -r requirements-private.txt

WORKDIR /app
ADD . /app


# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "--timeout", "0", "main:app"]