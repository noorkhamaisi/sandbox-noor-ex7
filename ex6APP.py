from google.cloud import firestore
import argparse
import csv
import seetree.gsutil as gsutil


client = firestore.Client(project="seetree-proto-dev")


def func(farmID,surveyID,dst_path):
    row=[]
    rows=[]

    cipo = client.collection(f"farms/{farmID}/rich-items").where(u"surveyID", u"==", u"{}".format(surveyID))
    cipo = cipo.where(u"richItemTypeName", u"==", u"cipo")
    results = cipo.stream()
    
    for task in results:
        row = []
        row.append(farmID)
        row.append(surveyID)
        groveID = task.get("groveID")
        row.append(groveID)
        row.append(client.document(f"farms/{farmID}").get().get("name"))
        row.append(client.document(f"farms/{farmID}/groves/{groveID}").get().get("name"))
        row.append(client.document(f"farms/{farmID}/surveys/{surveyID}").get().get("name"))
        row.append(task.get("geojsonPath"))
        rows.append(row)
     # open the file in the write mode
    with open('result-ex6.csv', 'w') as f:
        fieldnames = ["farmID","surveyID","groveID","farmName", "groveName", "surveyName", "geojsonPath"] 
        writer = csv.DictWriter(f,fieldnames=fieldnames)
        # write a row to the csv file
        writer.writeheader()
        for row in rows:
            writer.writerow({
                "farmID" : row[0],
                "surveyID" : row[1],
                "groveID" : row[2],
                "farmName" : row[3],
                "groveName" : row[4],
                "surveyName" : row[5],
                "geojsonPath" : row[6]
            })
    
    gsutil.cp('result-ex6.csv',dst_path)

if __name__ == "__main__":
     parser = argparse.ArgumentParser(description='run script')
     parser.add_argument('--farmID', required=True)
     parser.add_argument('--surveyID', required=True)
     parser.add_argument('--dst_path', required=False)

     args = parser.parse_args()
     func(args.farmID,args.surveyID,args.dst_path)